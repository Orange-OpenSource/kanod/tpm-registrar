#  Copyright (C) 2020-2021 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

from os import path
import base64
import requests
import subprocess
import time
import tempfile
import traceback
from typing import Union

from kanod_configure import common


def restart_network_dhcp(arg: common.RunnableParams):
    '''Replaces standard network start with start dhcp on all interfaces'''
    subprocess.run(['dhcp-all-interfaces.sh'])
    # previous process modifies /etc/network/interfaces and add configured
    # interfaces as auto.
    subprocess.run(['/sbin/ifup', '-a'])


common.register('DHCP all interfaces', 10, restart_network_dhcp)


def send_tpm_info(arg: common.RunnableParams):
    conf = arg.conf
    print('Generation of TPM context')
    tpm_config = conf.get('tpm', None)
    if tpm_config is None:
        return
    url = tpm_config.get('url', None)
    key = tpm_config.get('key', None)
    name = conf.get('name', None)

    ca = tpm_config.get('ca', None)
    if ca is None:
        verify: Union[str, bool] = True
    else:
        with tempfile.NamedTemporaryFile(
            'w', encoding='utf-8', delete=False
        ) as fd:
            verify = fd.name
            fd.write(ca)
            fd.write('\n')
    with tempfile.TemporaryDirectory() as tmpdir:
        ctxt_primary = path.join(tmpdir, 'primary.ctxt')
        ctxt_key = path.join(tmpdir, 'key.ctxt')
        key_pub = path.join(tmpdir, 'key.pub')
        key_priv = path.join(tmpdir, 'key.priv')
        key_pub_pem = path.join(tmpdir, 'pub.pem')
        try:
            print('Generate primary key')
            command = [
                'tpm2', 'createprimary', '-G', 'rsa', '-c', ctxt_primary]
            subprocess.run(command, check=True)
            print('Generate signing key')
            command = [
                'tpm2', 'create', '-G', 'rsa', '-C', ctxt_primary, '-a',
                ('noda|sign|fixedtpm|fixedparent|sensitivedataorigin'
                 '|userwithauth'),
                '-c', ctxt_key, '-u', key_pub, '-r', key_priv,
                '-f', 'pem', '-o', key_pub_pem]
            subprocess.run(command, check=True)
            print('prepare files')
            # No need to save the primary context. It must be recreated.
            files = [
                ('key.ctxt', open(ctxt_key, 'rb')),
                ('key.pub', open(key_pub, 'rb')),
                ('key.priv', open(key_priv, 'rb')),
                ('pub.pem', open(key_pub_pem, 'rb'))
            ]
            data = {'name': name, 'key': key}
        except Exception as e:
            # We got a problem. We send it back.
            traceback.print_exc()
            data = {'name': name, 'key': key, 'error': repr(e)}
            files = []
        print('Send back context and certificate files')
        while True:
            try:
                rep = requests.post(url, data=data, files=files, verify=verify)
                if rep.status_code == 204:
                    print('- sent back')
                    break
                print(f'- sending back failed ({rep.status_code}). retrying.')
            except Exception as e:
                print(f'- failure while sending back ({e}). retrying.')
            time.sleep(60)


common.register('Registrar enrolment', 200, send_tpm_info)
