#!/bin/bash

REPO_URL=${REPO_URL:-http://192.168.133.100/repository/kanod}
VERSION=${VERSION:-v0.1.0}
ARTIFACT=registrar

# shellcheck disable=SC2086
mvn ${MAVEN_CLI_OPTS} --batch-mode -B deploy:deploy-file ${MAVEN_OPTS} -DgroupId=kanod -DartifactId="${ARTIFACT}" -Dversion="${VERSION}" -Dpackaging=qcow2 -Dfile=registrar.qcow2 -Dfiles="manifest.yaml" -Dtypes=yml -Dclassifiers=manifest -Durl="${REPO_URL}" -DrepositoryId=kanod

