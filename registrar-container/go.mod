module gitlab.com/Orange-OpenSource/kanod/tpm-registrar

go 1.16

require (
	github.com/google/uuid v1.1.2
	github.com/hashicorp/vault/api v1.3.0
	github.com/metal3-io/baremetal-operator/apis v0.0.0-20211105090508-c38de6aabf99
	go.uber.org/zap v1.19.1
	gopkg.in/yaml.v2 v2.4.0
	k8s.io/api v0.22.2
	k8s.io/apimachinery v0.22.2
	k8s.io/client-go v0.22.2
	sigs.k8s.io/controller-runtime v0.10.3

)
