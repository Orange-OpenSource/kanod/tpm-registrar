/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package internal

import (
	"encoding/base64"
	"io"
	"net/http"
	"os"
	"path"

	"go.uber.org/zap"
)

var fileEntries = [...]string{
	"key.ctxt",
	"key.priv",
	"key.pub",
	"pub.pem",
}

func (s *RegistrarState) postRegister(w http.ResponseWriter, r *http.Request) {
	s.Logger.Info("Getting tpm info.")
	// Better be below 64K or Vault will not store it.
	r.ParseMultipartForm(1 << 16)
	result := make(map[string]string)
	name := r.FormValue("name")
	key := r.FormValue("key")
	regError := r.FormValue("error")
	if regError == "" {
		for _, fileEntry := range fileEntries {
			file, _, err := r.FormFile(fileEntry)
			if err != nil {
				s.Logger.Warn(
					"Cannot find component",
					zap.String("machine", name),
					zap.String("entry", fileEntry))
				continue
			}
			bytes, err := io.ReadAll(file)
			if err != nil {
				s.Logger.Warn(
					"Cannot read component",
					zap.String("machine", name),
					zap.String("entry", fileEntry))
				continue
			}
			result[fileEntry] = base64.StdEncoding.EncodeToString(bytes)
		}
	}
	s.Logger.Info("Parsed TPM elements", zap.String("machine", name))
	status, ok := s.BMH[name]
	if !ok {
		s.Logger.Error("Cannot find status", zap.String("machine", name))
		http.Error(w, "400 - No status", http.StatusBadRequest)
		return
	}
	if key != status.Key {
		s.Logger.Error("Wrong key", zap.String("machine", name))
		http.Error(w, "403 - Bad key", http.StatusForbidden)
		return
	}
	bmh, err := s.GetBMH(status.Namespace, name)
	if err != nil {
		s.Logger.Error(
			"Error while looking for BMH custom resource",
			zap.String("machine", name))
		http.Error(w, "500 - Finding BMH", http.StatusInternalServerError)
		return
	}
	if regError == "" {
		status.Registration = Registered
		s.Logger.Info("Write TPM elements to Vault", zap.String("machine", name))
		err = s.WriteVault(name, result)
		if err != nil {
			s.Logger.Error(
				"Error writing TPM info to Vault",
				zap.String("machine", name))
			http.Error(w, "500 - Vault access", http.StatusInternalServerError)
			status.Registration = TpmError
			status.Error = "cannot save TPM context on vault"
			return
		}
	} else {
		s.Logger.Error(
			"Error while looking for TPM",
			zap.String("error", regError),
			zap.String("machine", name))
		status.Registration = TpmError
		status.Error = regError
	}
	s.Logger.Info("Shutting down registrar image", zap.String("machine", name))
	err = s.UpdateBareMetal(bmh, status)
	if err != nil {
		http.Error(w, "500 - Update baremetal", http.StatusInternalServerError)
		return
	}
	w.WriteHeader(http.StatusNoContent)
}

func (s *RegistrarState) HandleHttp() error {
	addr := os.Getenv("REGISTRAR_ADDR")
	if addr == "" {
		addr = ":4400"
	}
	tlsFolder := os.Getenv("REGISTRAR_TLS_PATH")
	if tlsFolder == "" {
		tlsFolder = "/mnt/tls"
	}
	certPath := path.Join(tlsFolder, "tls.crt")
	keyPath := path.Join(tlsFolder, "tls.key")
	mux := http.NewServeMux()
	_, err1 := os.Stat(certPath)
	_, err2 := os.Stat(keyPath)
	mux.HandleFunc("/register", s.postRegister)
	if err1 == nil && err2 == nil {
		return http.ListenAndServeTLS(addr, certPath, keyPath, mux)
	}
	return http.ListenAndServe(addr, mux)
}
