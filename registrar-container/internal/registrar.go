/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package internal

import (
	"context"
	"fmt"
	"os"

	"github.com/google/uuid"
	baremetalhost "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	"go.uber.org/zap"
	yaml "gopkg.in/yaml.v2"
	v1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/api/errors"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/util/retry"
	ctrl "sigs.k8s.io/controller-runtime"
)

const BmhPausedAnnotation = "baremetalhost.metal3.io/paused"

// NewRegistrarState initializes the state of the operator.
//
// It creates a go-client and a vault clients, keeps a logger and a context.
// BareMetalHosts under scrutiny are saved in BMH field
func NewRegistrarState(logger *zap.Logger) (*RegistrarState, error) {
	ctx := context.Background()
	vaultClient, err := GetVaultClient(logger)
	if err != nil {
		return nil, err
	}
	kubeconfig := ctrl.GetConfigOrDie()
	k8sclient, err := GetK8SClient(kubeconfig, logger)
	if err != nil {
		return nil, err
	}
	bmhclient, err := GetBMHClient(kubeconfig, logger)
	if err != nil {
		return nil, err
	}
	nexusUrl := os.Getenv("NEXUS_URL")
	registrarVersion := os.Getenv("REGISTRAR_VERSION")
	if nexusUrl == "" {
		return nil, fmt.Errorf("need a value for NEXUS_URL")
	}
	if registrarVersion == "" {
		return nil, fmt.Errorf("need a value for REGISTRAR_VERSION")
	}
	imgUrl := fmt.Sprintf(
		"%s/repository/kanod/kanod/registrar/%s/registrar-%s.qcow2",
		nexusUrl, registrarVersion, registrarVersion)

	return &RegistrarState{
		Context:     ctx,
		VaultClient: vaultClient,
		Config:      kubeconfig,
		K8SClient:   k8sclient,
		BMHClient:   bmhclient,
		Logger:      logger,
		BMH:         make(map[string]*BmhRegistrationState),
		ImgUrl:      imgUrl,
	}, nil
}

// CreateUserData creates the secret containing UserData for a machine instance
//
// It wraps a Kanod configuration configData given as a string in a standard
// cloudInit and save it in the userData field of the secret. If the secret
// existed it is updated in case the configuration changed (the key for example)
func (s *RegistrarState) CreateUserData(
	name string,
	namespace string,
	configData string,
) (string, error) {
	userData := map[string]interface{}{
		"runcmd": []string{
			"kanod-runcmd",
			"echo $? > /etc/kanod-configure/status",
		},
		"write_files": []map[string]string{
			{
				"path":    "/etc/kanod-configure/configuration.yaml",
				"content": configData,
			},
		},
	}
	ymlData, err := yaml.Marshal(userData)
	if err != nil {
		return "", err
	}
	content := fmt.Sprintf("#cloud-config\n%s", ymlData)
	secretClient := s.K8SClient.CoreV1().Secrets(namespace)
	secretName := fmt.Sprintf("userdata-%s", name)
	secret := &v1.Secret{}
	secret.Name = secretName
	secret.Namespace = namespace
	secret.Data = make(map[string][]byte)
	secret.Data["userData"] = []byte(content)
	_, err = secretClient.Create(s.Context, secret, metav1.CreateOptions{})
	if errors.IsAlreadyExists(err) {
		s.Logger.Info(
			"User Data secret already exists. We update it",
			zap.String("machine", name))
		retryErr := retry.RetryOnConflict(retry.DefaultRetry, func() error {
			result, err := secretClient.Get(
				s.Context, secretName, metav1.GetOptions{})
			if err != nil {
				return err
			}
			result.Data["userData"] = []byte(content)
			_, updateErr := secretClient.Update(
				s.Context, result, metav1.UpdateOptions{})
			return updateErr
		})
		if retryErr != nil {
			return "", retryErr
		}
		return secretName, nil
	} else {
		if err != nil {
			return "", err
		}
		return secretName, nil
	}
}

func CreateConfigData(name string, key string) (string, error) {
	registrarUrl := os.Getenv("REGISTRAR_URL")
	registrarCa := os.Getenv("REGISTRAR_CA")
	if registrarCa == "" {
		registrarCa = "/mnt/registrar/ca.crt"
	}
	caData, err := os.ReadFile(registrarCa)
	if err != nil {
		return "", err
	}
	configData := map[string]interface{}{
		"name": name,
		"tpm": map[string]string{
			"url": registrarUrl,
			"key": key,
			"ca":  string(caData),
		},
	}
	y, err := yaml.Marshal(configData)
	return string(y), err
}

func (s *RegistrarState) UpdateBareMetal(
	bmh *baremetalhost.BareMetalHost,
	bs *BmhRegistrationState) error {

	var updateFn func(*baremetalhost.BareMetalHost) = nil
	switch bs.Registration {
	case Registering:
		appRoleId, err := s.CreateAppRole(bmh.Name)
		if err != nil {
			return err
		}
		configData, err := CreateConfigData(bmh.Name, bs.Key)
		if err != nil {
			return err
		}
		secretName, err := s.CreateUserData(bmh.Name, bmh.Namespace, configData)
		if err != nil {
			return err
		}
		updateFn = func(bmh *baremetalhost.BareMetalHost) {
			labels := bmh.Labels
			if labels == nil {
				labels = make(map[string]string)
				bmh.Labels = labels
			}
			labels["kanod.io/tpm-key"] = bs.Key
			labels["kanod.io/tpm-role-id"] = appRoleId
			labels["kanod.io/tpm"] = "registering"
			delete(labels, "kanod.io/tpm-error")
			bmh.Spec.Image = &baremetalhost.Image{
				URL:      s.ImgUrl,
				Checksum: s.ImgUrl + ".md5",
			}
			bmh.Spec.UserData = &v1.SecretReference{
				Name:      secretName,
				Namespace: bmh.Namespace,
			}
			bmh.Spec.Online = true
		}

	case Registered:
		updateFn = func(bmh *baremetalhost.BareMetalHost) {
			labels := bmh.Labels
			labels["kanod.io/tpm"] = "registered"
			delete(labels, "kanod.io/tpm-error")
			bmh.Spec.Image = nil
			bmh.Spec.UserData = nil
			bmh.Spec.Online = false
		}
	case TpmError:
		updateFn = func(bmh *baremetalhost.BareMetalHost) {
			labels := bmh.Labels
			delete(labels, "kanod.io/tpm")
			delete(labels, "kanod.io/tpm-role-id")
			delete(labels, "kanod.io/tpm-key")
			labels["kanod.io/tpm-error"] = bs.Error
			bmh.Spec.Image = nil
			bmh.Spec.UserData = nil
			bmh.Spec.Online = false
		}
	}
	err := s.UpdateBMH(bmh.Namespace, bmh.Name, updateFn)
	return err
}

func (s *RegistrarState) BareMetalAdded(bmh *baremetalhost.BareMetalHost) {
	// if baremetalhost contains the pause annotation, nothing is done
	if _, ok := bmh.ObjectMeta.Annotations[BmhPausedAnnotation]; ok {
		return
	}

	s.Logger.Info(
		"Add/Update BMH", zap.String("namespace", bmh.Namespace),
		zap.String("name", bmh.Name))
	bs, present := s.BMH[bmh.Name]
	isRegistering := (bmh.Labels != nil && bmh.Labels["kanod.io/tpm"] != "")
	isReady := (bmh.Status.Provisioning.State == baremetalhost.StateReady || bmh.Status.Provisioning.State == baremetalhost.StateAvailable)
	if !present && isRegistering {
		s.Logger.Info(
			"Updating internal state for BMH", zap.String("namespace", bmh.Namespace),
			zap.String("name", bmh.Name))
		key := bmh.Labels["kanod.io/tpm-key"]
		var state TpmRegistration
		switch bmh.Labels["kanod.io/tpm"] {
		case "registering":
			state = Registering
		case "registered":
			state = Registered
		case "no-tpm":
			state = NoTpm
		default:
			state = TpmError
		}
		bs = &BmhRegistrationState{
			Key:          key,
			Registration: state,
			Namespace:    bmh.Namespace,
		}
		s.BMH[bmh.Name] = bs
	} else if !present && isReady {
		s.Logger.Info(
			"Registering BMH", zap.String("namespace", bmh.Namespace),
			zap.String("name", bmh.Name))
		key := uuid.New().String()
		bs = &BmhRegistrationState{
			Key:          key,
			Registration: Registering,
			Namespace:    bmh.Namespace,
		}
		s.BMH[bmh.Name] = bs
		err := s.UpdateBareMetal(bmh, bs)
		if err != nil {
			s.Logger.Error(
				"Cannot update BareMetalHost",
				zap.String("machine", bmh.Name), zap.Error(err))
		}
	}
	if bs != nil && bs.Registration == Registered {
		policy := ""
		consumer := bmh.Spec.ConsumerRef
		if consumer != nil && consumer.Kind == "Metal3Machine" {
			if bs.Consumer == consumer.Name {
				// Already seen. We stop here.
				// Strong assumption: Machines are immutable and belong to a
				// cluster.
				return
			}
			var err error
			policy, err = s.MachinePolicy(consumer)
			if err != nil {
				s.Logger.Error(
					"Failed to retrieve cluster name as policy name",
					zap.String("machine", bmh.Name), zap.Error(err))
				// Should we reset the policy to default if sthing goes wrong
				// Could help to prevent attack while jeopardizing
				// cluster reliability
				return
			}
		} else {
			// ensure reset
			bs.Consumer = ""
		}
		if bs.Policy != policy {
			s.AssignPolicy(bmh.Name, policy)
			bs.Policy = policy
		}
	}
}

func (s *RegistrarState) BareMetalDeleted(bmh *baremetalhost.BareMetalHost) {
	// if baremetalhost contains the pause annotation, nothing is done
	if _, ok := bmh.ObjectMeta.Annotations[BmhPausedAnnotation]; ok {
		return
	}
	s.Logger.Info(
		"Delete BMH", zap.String("namespace", bmh.Namespace),
		zap.String("name", bmh.Name))
	delete(s.BMH, bmh.Name)
}

func (state *RegistrarState) WatchBMH() {

	BaremetalHostWatcher(
		state.Config,
		func(obj *baremetalhost.BareMetalHost) { state.BareMetalAdded(obj) },
		func(_ *baremetalhost.BareMetalHost, obj *baremetalhost.BareMetalHost) {
			state.BareMetalAdded(obj)
		},
		func(obj *baremetalhost.BareMetalHost) { state.BareMetalDeleted(obj) },
	)
}
