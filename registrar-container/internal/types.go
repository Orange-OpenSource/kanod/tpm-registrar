/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package internal

import (
	"context"

	vaultapi "github.com/hashicorp/vault/api"
	"go.uber.org/zap"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/rest"
)

type TpmRegistration int

const (
	SecretCreating TpmRegistration = iota
	Registering
	Registered
	NoTpm
	TpmError
)

func TpmRegistrationString(tr TpmRegistration) string {
	switch tr {
	case SecretCreating:
		return "creating"
	case Registering:
		return "registering"
	case Registered:
		return "registered"
	case NoTpm:
		return "no-tpm"
	case TpmError:
		return "tpm-errror"
	default:
		return "unknown"
	}

}

type BmhRegistrationState struct {
	Registration TpmRegistration
	Key          string
	Namespace    string
	Policy       string
	Consumer     string
	Error        string
}

type RegistrarState struct {
	Context     context.Context
	VaultClient *vaultapi.Client
	Config      *rest.Config
	BMHClient   *rest.RESTClient
	K8SClient   *kubernetes.Clientset
	Logger      *zap.Logger
	BMH         map[string]*BmhRegistrationState
	ImgUrl      string
}
