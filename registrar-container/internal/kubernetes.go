/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package internal

import (
	"fmt"

	"go.uber.org/zap"
	v1 "k8s.io/api/core/v1"
	clientgoscheme "k8s.io/client-go/kubernetes/scheme"

	baremetalhost "github.com/metal3-io/baremetal-operator/apis/metal3.io/v1alpha1"
	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/apimachinery/pkg/apis/meta/v1/unstructured"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/runtime/schema"
	"k8s.io/apimachinery/pkg/runtime/serializer"
	"k8s.io/client-go/dynamic"
	"k8s.io/client-go/dynamic/dynamicinformer"
	"k8s.io/client-go/kubernetes"
	"k8s.io/client-go/metadata"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/cache"
	"k8s.io/client-go/util/retry"
)

func GetK8SClient(kubeconfig *rest.Config, logger *zap.Logger) (*kubernetes.Clientset, error) {
	logger.Info("Creating kubernetes client")
	k8sclient, err := kubernetes.NewForConfig(kubeconfig)
	return k8sclient, err
}

func GetBMHClient(
	kubeconfig *rest.Config, logger *zap.Logger,
) (*rest.RESTClient, error) {
	logger.Info("Creating client for baremetal host")
	config := rest.CopyConfig(kubeconfig)
	config.ContentConfig.GroupVersion = &baremetalhost.GroupVersion
	config.APIPath = "/apis"
	baremetalhost.AddToScheme(clientgoscheme.Scheme)
	config.NegotiatedSerializer = serializer.NewCodecFactory(clientgoscheme.Scheme)
	config.UserAgent = rest.DefaultKubernetesUserAgent()
	client, err := rest.UnversionedRESTClientFor(config)
	return client, err
}

func GetDynamicInformer(
	cfg *rest.Config, resourceType string,
) (cache.SharedIndexInformer, error) {
	dynItf, err := dynamic.NewForConfig(cfg)
	if err != nil {
		return nil, err
	}
	factory := dynamicinformer.NewDynamicSharedInformerFactory(
		dynItf,
		0, // resync
	)
	gvr, _ := schema.ParseResourceArg(resourceType)
	informer := factory.ForResource(*gvr).Informer()
	return informer, nil
}

func (s *RegistrarState) GetBMH(namespace string, name string) (*baremetalhost.BareMetalHost, error) {
	result := &baremetalhost.BareMetalHost{}
	err := s.BMHClient.Get().Namespace(namespace).Resource("baremetalhosts").Name(name).Do(s.Context).Into(result)
	return result, err
}

func (s *RegistrarState) UpdateBMH(
	namespace string, name string, updateFn func(*baremetalhost.BareMetalHost),
) error {
	return retry.RetryOnConflict(
		retry.DefaultRetry,
		func() error {
			bmh, err := s.GetBMH(namespace, name)
			if err != nil {
				return err
			}
			s.Logger.Info(
				"updating baremetal host",
				zap.String("machine", name), zap.Bool("State", bmh.Spec.Online))
			updateFn(bmh)
			err = s.BMHClient.Put().Namespace(bmh.Namespace).Resource("baremetalhosts").Name(bmh.Name).Body(bmh).Do(s.Context).Error()
			return err
		},
	)
}

func toBMH(obj interface{}) *baremetalhost.BareMetalHost {
	bmh := &baremetalhost.BareMetalHost{}
	err := runtime.DefaultUnstructuredConverter.FromUnstructured(
		obj.(*unstructured.Unstructured).UnstructuredContent(),
		bmh)
	if err != nil {
		return nil
	}
	return bmh
}

type bmhFunc func(obj *baremetalhost.BareMetalHost)
type bmhFunc2 func(
	obj1 *baremetalhost.BareMetalHost,
	obj2 *baremetalhost.BareMetalHost)

func BaremetalHostWatcher(
	cfg *rest.Config,
	addFunc bmhFunc, updFunc bmhFunc2, delFunc bmhFunc,
) error {
	bmhInformer, err := GetDynamicInformer(
		cfg, "baremetalhosts.v1alpha1.metal3.io")
	if err != nil {
		return err
	}
	stopper := make(chan struct{})
	defer close(stopper)
	handlers := cache.ResourceEventHandlerFuncs{
		AddFunc: func(obj interface{}) {
			addFunc(toBMH(obj))
		},
		DeleteFunc: func(obj interface{}) {
			delFunc(toBMH(obj))
		},
		UpdateFunc: func(oldObj, newObj interface{}) {
			updFunc(toBMH(oldObj), toBMH(newObj))
		},
	}
	bmhInformer.AddEventHandler(handlers)
	bmhInformer.Run(stopper)
	return nil
}

func (s *RegistrarState) MachinePolicy(ref *v1.ObjectReference) (string, error) {
	client, err := metadata.NewForConfig(s.Config)
	if err != nil {
		return "", err
	}
	meta, err := client.Resource(schema.GroupVersionResource{
		Group:    ref.GroupVersionKind().Group,
		Version:  ref.GroupVersionKind().Version,
		Resource: "metal3machines",
	}).Namespace(ref.Namespace).Get(s.Context, ref.Name, metav1.GetOptions{})
	if err != nil {
		return "", err
	}
	labels := meta.Labels
	if labels == nil {
		return "", fmt.Errorf("no labels in metal3machine %s.%s",
			ref.Namespace, ref.Name)
	}
	policy := labels["cluster.x-k8s.io/cluster-name"]
	s.Logger.Info(
		"Found policy for metal3machine",
		zap.String("namespace", ref.Namespace),
		zap.String("m3machine", ref.Name),
		zap.String("cluster", policy))
	return policy, nil
}
