/*
Copyright 2021 Orange.

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package internal

import (
	"fmt"
	"io"
	"os"

	vaultapi "github.com/hashicorp/vault/api"

	"go.uber.org/zap"
)

func getJwtToken() (string, error) {
	tokenFile := "/var/run/secrets/kubernetes.io/serviceaccount/token"
	fd, err := os.Open(tokenFile)
	if err != nil {
		return "", err
	}
	defer fd.Close()
	bytes, err := io.ReadAll(fd)
	if err != nil {
		return "", err
	}
	return string(bytes), nil
}

func getVaultToken(logger *zap.Logger, client *vaultapi.Client) error {
	token, err := getJwtToken()
	if err != nil {
		return err
	}
	vault_role := os.Getenv("VAULT_ROLE")
	if vault_role == "" {
		vault_role = "registrar"
	}
	payload := map[string]interface{}{
		"role": vault_role,
		"jwt":  token,
	}
	k8s := os.Getenv("VAULT_K8S")
	if k8s == "" {
		k8s = "lcm"
	}
	url := fmt.Sprintf("auth/kubernetes/%s/login", k8s)
	result, err := client.Logical().Write(url, payload)
	if err != nil {
		logger.Info("Getting a token failed", zap.Error(err))
		return err
	}
	client.SetToken(result.Auth.ClientToken)
	return nil
}

func GetVaultClient(logger *zap.Logger) (*vaultapi.Client, error) {
	if os.Getenv("VAULT_CACERT") == "" {
		os.Setenv("VAULT_CACERT", "/mnt/vault/ca.crt")
	}
	vaultClient, err := vaultapi.NewClient(vaultapi.DefaultConfig())
	if err != nil {
		logger.Error("Cannot create vault client", zap.Error(err))
		return nil, err
	}
	err = getVaultToken(logger, vaultClient)
	if err != nil {
		return nil, err
	}
	return vaultClient, nil
}

// CreateAppRole creates a new approle on Vault for a machine

func (s *RegistrarState) CreateAppRole(name string) (string, error) {
	s.Logger.Info("Creating approle", zap.String("machine", name))
	path := fmt.Sprintf("auth/approle/role/%s", name)
	data := map[string]interface{}{
		"token_policies": []string{},
	}
	// Check if the path exists. Create it only if error.
	_, err := s.VaultClient.Logical().Read(path)
	if err != nil {
		_, err := s.VaultClient.Logical().Write(path, data)
		if err != nil {
			s.Logger.Error(
				"Registration of approle failed",
				zap.String("machine", name), zap.Error(err))
			return "", err
		}
	} else {
		s.Logger.Info("Approle existed", zap.String("machine", name))
	}
	path = fmt.Sprintf("%s/role-id", path)
	resp, err := s.VaultClient.Logical().Read(path)
	if err != nil {
		s.Logger.Error(
			"Reading approle role-id failed",
			zap.String("machine", name), zap.Error(err))
		return "", err
	}
	result := resp.Data["role_id"].(string)
	return result, nil
}

// AssignPolicy assigns a policy to an approle. If policy name is
// empty, then the default policy is assigned.
func (s *RegistrarState) AssignPolicy(approle string, policy string) error {
	s.Logger.Info(
		"Assign cluster policy",
		zap.String("machine", approle), zap.String("policy", policy))
	path := fmt.Sprintf("/auth/approle/role/%s/policies", approle)
	if policy == "" {
		policy = "default"
	}
	policies := [1]string{policy}
	params := make(map[string]interface{})
	params["token_policies"] = policies
	_, err := s.VaultClient.Logical().Write(path, params)
	return err
}

// DeleteAppRole delete a machine approle (if the machine is deleted)
func (s *RegistrarState) DeleteAppRole(name string) error {
	s.Logger.Info("Delete approle", zap.String("machine", name))
	path := fmt.Sprintf("auth/approle/role/%s", name)
	_, err := s.VaultClient.Logical().Delete(path)
	if err != nil {
		s.Logger.Error(
			"Deletion of approle failed",
			zap.String("machine", name), zap.Error(err))
		return err
	}
	return nil
}

func (s *RegistrarState) WriteVault(name string, entries map[string]string) error {
	data := make(map[string]interface{})
	data["data"] = entries
	url := fmt.Sprintf("kv/data/machines/%s", name)
	_, err := s.VaultClient.Logical().Write(url, data)
	return err
}
