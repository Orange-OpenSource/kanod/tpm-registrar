$schema: http://json-schema.org/draft-07/schema#
additionalProperties: false
definitions:
  admin_user_definition:
    additionalProperties: false
    description: Configuration of the admin user of the machine.
    properties:
      keys:
        items:
          description: ssh public key accepted to log as admin user.
          type: string
        type: array
      passwd:
        description: optional password for admin user (do not use in production)
        type: string
    type: object
  mount_point_definition:
    additionalItems: false
    description: 'This option is used to mount additional disks

      on the root file-system. For virtual machines, disks are usually names

      vdX and vdb is the first additional disk. For real servers, the name

      depend on the storage technology.

      '
    items:
    - description: a source device (eg /dev/vdb)
      type: string
    - description: a mount point (eg /data)
      type: string
    type: array
  network_check:
    description: 'Network checks are used to block cloud-init execution until the

      network is available. Various kind of checks are provided: accessing

      an http server, establishing a regular TCP connection or querying a

      DNS server.

      '
    oneOf:
    - additionalProperties: false
      properties:
        http:
          description: a URL to reach
          type: string
        tries:
          description: number of tries to perform (-1 = unbounded)
          type: integer
      required:
      - http
      type: object
    - additionalProperties: false
      properties:
        tcp:
          description: a port on a server to connect to specified as address:port
          type: string
        tries:
          description: number of tries to perform (-1 = unbounded)
          type: integer
      required:
      - tcp
      type: object
    - additionalProperties: false
      properties:
        dns:
          description: a host name to solve
          type: string
        tries:
          description: number of tries to perform (-1 = unbounded)
          type: integer
      required:
      - dns
      type: object
  ntp_config_definition:
    additionalProperties: false
    description: Configuration of the configuration service.
    properties:
      clients:
        description: a list of network in cidr format served by this NTP server
        items:
          type: string
        type: array
      pools:
        items:
          description: a list of NTP pools providing clock synchronization.
          type: string
        type: array
      servers:
        items:
          description: a list of NTP servers providing clock synchronization.
          type: string
        type: array
    type: object
  proxy_definition:
    additionalProperties: false
    properties:
      http:
        description: the URL of the http proxy if there is one
        format: uri
        type: string
      https:
        description: the URL for the https proxy if there is one
        format: uri
        type: string
      no_proxy:
        description: 'a comma separated of domain suffix that should not go through

          the proxy

          '
        type: string
    type: object
  tpm_registrar_configuration:
    properties:
      ca:
        description: CA used by the registrar for https comms
        type: string
      key:
        description: Authentication key (used as a password when sending back)
        type: string
      url:
        description: Url of the registrar
        type: string
    type: object
description: 'This machine is "transient" and has little configuration.

  All it does is to create a TPM key that can be later used to authenticate

  the machine itself.

  '
properties:
  admin:
    $ref: '#/definitions/admin_user_definition'
  certificates:
    additionalProperties:
      type: string
    description: 'A dictionary where keys are certificate names and values certificates

      in PEM format.

      '
    type: object
  mounts:
    description: 'list of mount points.

      '
    items:
      $ref: '#/definitions/mount_point_definition'
    type: array
  name:
    description: name of the machine launched
    type: string
  network:
    description: 'Definition of the network configuration in cloudinit version 2 format


      This is a simplified version of the netplan configuration.


      Please refer to cloudinit documentation.


      (https://cloudinit.readthedocs.io/en/latest/topics/network-config-format-v2.html)

      '
    type: object
  network_checks:
    items:
      $ref: '#/definitions/network_check'
    type: array
  ntp:
    $ref: '#/definitions/ntp_config_definition'
  proxy:
    $ref: '#/definitions/proxy_definition'
  tpm_registrar:
    $ref: '#/definitions/tpm_registrar_configuration'
required:
- name
- name
- network
- tpm_registrar
title: TPM Registrar Configuration
type: object
