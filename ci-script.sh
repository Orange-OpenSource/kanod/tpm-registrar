#!/bin/bash

#  Copyright (C) 2022 Orange
#
#    Licensed under the Apache License, Version 2.0 (the "License"); you may
#    not use this file except in compliance with the License. You may obtain
#    a copy of the License at
#
#         http://www.apache.org/licenses/LICENSE-2.0
#
#    Unless required by applicable law or agreed to in writing, software
#    distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
#    WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
#    License for the specific language governing permissions and limitations
#    under the License.

set -eu

"${PYTHON:-/usr/bin/python3}" -m pip install diskimage-builder

export ARTIFACT=registrar
export CONTAINER=tpm-registrar

for var in NEXUS_KANOD_USER NEXUS_REGISTRY REPO_URL VERSION
do
    if ! [[ -v "${var}" ]]; then
        echo "${var} must be defined"
        exit
    fi
done

./make-image.sh

# shellcheck disable=SC2086
mvn ${MAVEN_CLI_OPTS:-} --batch-mode -B deploy:deploy-file ${MAVEN_OPTS:-} -DgroupId=kanod -DartifactId="${ARTIFACT}" -Dversion="${VERSION}" -Dpackaging=qcow2 -Dfile=registrar.qcow2 -Dfiles="manifest.yaml" -Dtypes=yml -Dclassifiers=manifest -Durl="${REPO_URL}" -DrepositoryId=kanod

cd registrar-container || exit 1

docker build --build-arg "http_proxy=${http_proxy:-}" --build-arg "https_proxy=${https_proxy:-}" --build-arg "no
_proxy=${no_proxy:-}"  . -t "$NEXUS_REGISTRY/${CONTAINER}:$VERSION"

echo "${NEXUS_KANOD_PASSWORD}" | docker login -u "${NEXUS_KANOD_USER}" --password-stdin "${NEXUS_REGISTRY}"

docker push "$NEXUS_REGISTRY/${CONTAINER}:$VERSION"
