Registrar container
===================
Environment variables
---------------------

VAULT_K8S
    Identification of the Kubernetes server where the registrar run on Vault

VAULT_ROLE
    Vault role used by the registrar to authenticate itself

VAULT_CA
    Path to the file containing the certificate of the CA used by Vault.

REGISTRAR_ADDR
    Local internal address of the nexus (used to override port or limit 
    visibility on interfaces)

REGISTRAR_TLS_PATH
    Path to the folder containing ``tls.crt`` and ``tls.key``

NEXUS_URL
    URL to the Maven repository holding OS images.

REGISTRAR_VERSION
    Version of the registrar (used to compute the exact image URL on Nexus)

REGISTRAR_URL
    URL of the registrar on the server network.

REGISTRAR_CA
    Path to the file containing the certificate of the CA used
    externally by the registrar (as it is seen by servers).

BMH_NAMESPACE
    Namespace where baremetal hosts are declared.


Files
-----

``/mnt/tls/tls.crt`` and ``/mnt/tls/tls.crt``
    certificate and key for the registrar web server. If not provided, traffic
    is served over http and a reverse proxy should be used.

``/mnt/vault/ca.crt``
    certificate authority to connect to the Vault instance

``/mnt/registrar/ca.crt``
    certificate authority used by the registrar as seen by the machines
    registering on it.