Registrar Image
---------------
This image is used to create a keypair in the TPM of a server export its context and certificate to Vault so that it can later be used to authenticate the server.

There is no reason to use this image in another context, so configuration options are limited.
The image uses DHCP on all interfaces to configure access to the network
and to be able to call back the registrar.

.. jsonschema:: schema.yaml
    :lift_title:
    :lift_description:
    :lift_definitions:
    :auto_reference:
    :auto_target: